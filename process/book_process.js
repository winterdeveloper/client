var request = require('request');
var booksDao = require('../dhandle/book.dhandle.js');

module.exports = { 
  search : function (request, callback) {
    const isbnNo = request.params.isbnNo;
    if (isbnNo === undefined) {
      callback(new Error('missing isbn query parameter ' +  JSON.stringify(isbnNo)))
      return
    }
    const bookParams = isbnNo + "+isbn"
    const url = 'https://www.googleapis.com/books/v1/volumes'
    const query_string = {q: bookParams, maxResults: 40, fields: 'items(id,volumeInfo(title,authors, categories, industryIdentifiers))'}
    
    module.exports.getData(url, query_string,function(err, data) {
        if (err) {
          callback(new Error(err.message))
          return
        }
        const items = JSON.parse(data).items
        const books = items.map( element => {
          return {id:element.id, 
                  category:element.volumeInfo.categories,
                  title:element.volumeInfo.title, 
                  authors:element.volumeInfo.authors,
                  industryIdentifiers: element.volumeInfo.industryIdentifiers
          }
        })
        callback(null, books)
      })
    }, 
  getData(url, queryString, callback) {
    request.get({url: url, qs: queryString}, (err, res, body) => {
        if (err) {
          callback(new Error('request not successful'))
        }
        callback(null, body)
      })
  },
  getBook : function (request, callback) {
      const bookId = request.params.id;
      if (bookId === '') {
        callback(new Error('missing book id query parameter'))
        return
      }
      var books = {id: bookId};
			booksDao.queryGetBook(books).then(function (row) {
			  var ouput = { 
			    response: "success",
  			  items: row
			  }
			  
       callback(null, ouput);
			  
			});
    
  },
  getAllBooks : function (request, callback) {
			booksDao.queryGetALLBooks().then(function (row) {
			  var ouput = { 
			    response: "success",
  			  items: (row)
			  }
			  
       callback(null, ouput);
			  
			});
    
  },
  createBook : function (request, callback) {
      var bookData = {
        title: request.body.title,
        author: request.body.author,
        google_id : request.body.google_id,
        category: request.body.category
      }
		  var book = 	booksDao.buildItem(bookData)
		  book.save().then(function(ouput) {
        callback(null, ouput);
		  });
    
  },
  deleteBook : function (request, callback) {
     const bookId = request.params.id;
      var bookData = {
        id: bookId
      }
		  var book = 	booksDao.queryDeleteBook(bookData).then(function(ouput) {
		    var result = {
		      message: 'success'
		    }
        callback(null, result);
		  });
    
  }
  
}
