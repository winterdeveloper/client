var request = require('request');
var usersDao = require('../dhandle/users.dhandle.js');

module.exports = { 
    loginUser : function (request, callback) {
	  const username = request.body.username;
	  const password = request.body.password;
	  console.log(request.body);
      if (!username || !password || username === '' || password === '') {
        callback(new Error('Invalid username or password.'))
        return
      }
      var users = {username: username, password: password};
		usersDao.queryLogin(users).then(function (row) {
		  var ouput ={ response: "success"}
          callback(null, ouput);
	    });
    },
    getUser : function (request, callback) {
	  const id = request.params.id;
      if (id === '') {
        callback(new Error('User id not specified.'))
        return
      }
      var users = {id: id};
		usersDao.queryUser(users).then(function (row) {
		  var ouput ={ response: "success", user: row}
          callback(null, ouput);
	    });
    },    
    createUser : function (request, callback) {
        if (request.body.username === '' || request.body.username === '' || request.body.firstName  === '' || request.body.lastName  === '') {
            callback(new Error('Invalid data.'))
            return
          }
          var usersData = {
            username: request.body.username,
            password: request.body.password,
            first_name : request.body.firstName,
            last_name: request.body.lastName,
            email: request.body.email
          }
    	  var users = 	usersDao.buildUser(usersData)
    	  users.save().then(function(ouput) {
            callback(null, ouput);
    	  });
    },
    getAllUsers : function (request, callback) {
        usersDao.queryGetALLUsers().then(function (row) {
            var ouput = { 
                response: "success",
                items: (row)
            }
            callback(null, ouput);
        });
    
    },
    updateUser: function (request, callback) {
        
    },
    deleteUser: function (request, callback) {
        
    }
}
