
var connection = require('../database/connection.js');

module.exports = {
    close: function () {
    	var sequelize = connection.getSequelize();
    	sequelize.close();
    },
    buildItem: function (data) {
    	var BooksModel = connection.getSequelize("books");
    	return BooksModel.build(data);
    },
    queryGetBook: function (data) {
    	var BooksModel = connection.getSequelize("books");
    	return BooksModel.findOne({
    	    where: {id: data.id}
    	}); 
    },
    queryDeleteBook:  function (data) {
    	var BooksModel = connection.getSequelize("books");
    	return BooksModel.destroy({
            where: {
                id: data.id
            }
        })
    },
    queryGetALLBooks: function () {
    	var BooksModel = connection.getSequelize("books");
    	return BooksModel.findAll(); 
    },
}