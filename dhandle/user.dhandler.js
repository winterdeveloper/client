
var connection = require('../database/connection.js');

module.exports = {
    close: function () {
    	var sequelize = connection.getSequelize();
    	sequelize.close();
    },
    buildUser: function (data) {
    	var UsersModel = connection.getSequelize("users");
    	return UsersModel.build(data);
    },
    queryUser: function (data) {
    	var UsersModel = connection.getSequelize("users");
    	return UsersModel.findOne({
    	    where: {id: data.id}
    	}); 
    },
    queryLogin: function (data) {
    	var UsersModel = connection.getSequelize("users");
    	return UsersModel.findOne({
    	    where: {username: data.username, password: data.password}
    	}); 
    },
    queryGetALLUsers: function () {
    	var UsersModel = connection.getSequelize("users");
    	return UsersModel.findAll(); 
    },
}