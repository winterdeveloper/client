var Sequelize=require("sequelize");
var sequelize=null;

var mysql=require('mysql');
var pool=null;

module.exports={
    getSequelize: function (model) {
        var importModels=function (sequelize, model) {
            return sequelize.import("../_model/c9/" + model);
        }
        if (!sequelize && module.exports.DATABASE !== null) {
            sequelize=new Sequelize("c9", "winterdeveloper", "", {
                host: "localhost",
                dialect: 'mysql',
                dialectOptions: {
                    insecureAuth: true
                },
                define: {
                    timestamps: false,
                },
                pool: {
                    max: 5,
                    min: 0,
                    idle: 300
                },
            });
        }
        if (model) {
            return importModels(sequelize, model);
        }
        return sequelize;
    }
}
