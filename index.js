/* import the 'restify' module and create an instance. */
var restify = require('restify');
var fs = require("fs")
var server = restify.createServer();

/* import the required plugins to parse the body and auth header. */
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());


server.get("/books/isbn/:isbnNo", function (req, res, next) {
  var books = require('./process/books_process')
  books.search(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code, response)
    return next();
  })
});

server.get("/books/:id", function (req, res, next) {
  var books = require('./process/books_process')
  books.getBook(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});

//Create book
server.post("/books", function (req, res, next) {
  var books = require('./process/books_process')
  books.createBook(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});

//Delete books
server.del("/books/:id", function (req, res, next) {
  var books = require('./process/books_process')
  books.deleteBook(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code, response)
    return next();
  })
});

 //Get list of books
server.get("/books", function (req, res, next) {
  var books = require('./process/books_process')
  books.getAllBooks(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});

//Get all users endpoint
server.get("/users", function (req, res, next) {
  var users = require('./user.js')
  users.getAllUsers(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});

//Get user endpoint
server.get("/users/:id", function (req, res, next) {
  var users = require('./user.js')
  users.getUser(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});

//Create user endpoint
server.post("/users", function (req, res, next) {
  var users = require('./user')
  users.createUser(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});

//Login endpoint
server.post("/login", function (req, res, next) {
  var users = require('./user.js')
  users.loginUser(req, (err, data) => {
    var code, response
    if (err) {
      console.log('error')
      code = 400
      response = err
    } else {
      code = 200
      response = data
    }
    res.setHeader('content-type', 'application/json')
    res.send(code,  response)
    return next();
  })
});
 
var port = process.env.PORT || 8080;
server.listen(port, function (err) {
    if (err)
        console.error(err)
    else
        console.log('App is ready at : ' + port)
})
 
if (process.env.environment == 'production') {
    process.on('uncaughtException', function (err) {
        console.error(JSON.parse(JSON.stringify(err, ['stack', 'message', 'inner'], 2)))
    })
}
